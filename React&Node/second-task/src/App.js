import Video from "./component/Video";

function App() {
  const video = document.getElementById('video');
  return (
    <div className="container">
      <Video video={video}/>
    </div>
  );
}

export default App;
