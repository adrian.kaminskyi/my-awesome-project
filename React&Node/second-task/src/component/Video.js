import React from 'react'

export default function Video(video, audio) {

    const startUp = ()  => {
        navigator.mediaDevices.getUserMedia({
            audio: true,
            video: true
        }).then(stream => {
            video.srcObject = stream;
            audio.srcObject = stream;
        }).catch(console.error)
    }

    window.addEventListener('load', startUp, false)
    if(navigator.mediaDevices.getUserMedia({
        audio:true
    })) {
        return (
            <div className="row">
                <video itemID="video" autoPlay className="video" />
                <div className="square-green"></div>
            </div>
        )
    } else {
        return (
            <div className="row">
                <video itemID="video" autoPlay className="video" />
                <div className="square"></div>
            </div>
        )
    }
    
}
